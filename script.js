const searchBtn = document.getElementById('search-btn');
const mealList = document.getElementById('meal');
const mealDetailsContent = document.querySelector('.meal-details-content');
const recipeCloseBtn = document.getElementById('recipe-close-btn');

// event listeners
searchBtn.addEventListener('click', getMealList);
mealList.addEventListener('click', getMealRecipe);
recipeCloseBtn.addEventListener('click', () => {
    mealDetailsContent.parentElement.classList.remove('showRecipe');
});


// get meal list that matches with the ingredients
// Get meal list that matches with the ingredients
function getMealList() {
    let searchInputTxt1 = document.getElementById('search-input-1').value.trim();
    let searchInputTxt2 = document.getElementById('search-input-2').value.trim();

    fetch(`https://www.themealdb.com/api/json/v1/1/filter.php?i=${searchInputTxt1}`)
        .then(response => response.json())
        .then(data => {
            const meals1 = data.meals;
            if (meals1) {
                // If the first ingredient has results, fetch results for the second ingredient
                fetch(`https://www.themealdb.com/api/json/v1/1/filter.php?i=${searchInputTxt2}`)
                    .then(response => response.json())
                    .then(data => {
                        const meals2 = data.meals;

                        // Filter meals that have both ingredients
                        const matchingMeals = meals1.filter(meal1 =>
                            meals2.some(meal2 => meal1.idMeal === meal2.idMeal)
                        );

                        let html = "";
                        if (matchingMeals.length > 0) {
                            matchingMeals.forEach(meal => {
                                html += `
                                    <div class="meal-item" data-id="${meal.idMeal}">
                                        <div class="meal-img">
                                            <img src="${meal.strMealThumb}" alt="food">
                                        </div>
                                        <div class="meal-name">
                                            <h3>${meal.strMeal}</h3>
                                            <a href="#" class="recipe-btn">Get Recipe</a>
                                        </div>
                                    </div>
                                `;
                            });
                            mealList.classList.remove('notFound');
                        } else {
                            html = "Sorry, we didn't find any meal with these ingredients!";
                            mealList.classList.add('notFound');
                        }

                        mealList.innerHTML = html;
                    });
            } else {
                const html = "Sorry, we didn't find any meal with the first ingredient!";
                mealList.innerHTML = html;
                mealList.classList.add('notFound');
            }
        });
}



// get recipe of the meal
function getMealRecipe(e){
    e.preventDefault();
    if(e.target.classList.contains('recipe-btn')){
        let mealItem = e.target.parentElement.parentElement;
        fetch(`https://www.themealdb.com/api/json/v1/1/lookup.php?i=${mealItem.dataset.id}`)
        .then(response => response.json())
        .then(data => mealRecipeModal(data.meals));
    }
}

// create a modal
function mealRecipeModal(meal){
    console.log(meal);
    meal = meal[0];
    let html = `
        <h2 class = "recipe-title">${meal.strMeal}</h2>
        <p class = "recipe-category">${meal.strCategory}</p>
        <div class = "recipe-instruct">
            <h3>Instructions:</h3>
            <p>${meal.strInstructions}</p>
        </div>
        <div class = "recipe-meal-img">
            <img src = "${meal.strMealThumb}" alt = "">
        </div>
        <div class = "recipe-link">
            <a href = "${meal.strYoutube}" target = "_blank">Watch Video</a>
        </div>
    `;
    mealDetailsContent.innerHTML = html;
    mealDetailsContent.parentElement.classList.add('showRecipe');
}